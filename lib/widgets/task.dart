import 'package:flutter/material.dart';
import 'package:persistance_app/models/Task.dart';

class TaskState extends State<TaskWidget> {
  bool _editMode = false;
  Icon _icon = Icon(Icons.edit);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('View'),
        actions: <Widget>[
          IconButton(icon: _icon, onPressed: _switchEditMode),
        ],
      ),
      body: _buildTask(),
    );
  }

  void _switchEditMode() {
    setState(() {
      _editMode = !_editMode;
      _icon = _editMode ? Icon(Icons.close) : Icon(Icons.edit);
    });
  }

  Widget _buildTask() {

    if (_editMode) {
      return Container(
          margin: EdgeInsets.all(16.0),
          child: Form(
              key: _formKey,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget> [
                    TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Task name',
                        border: OutlineInputBorder(),
                        prefixIcon: Icon(Icons.info),

                      ),
                      keyboardType: TextInputType.text,
                      initialValue: widget.task.name != null && widget.task.name.isNotEmpty
                          ? widget.task.name
                          : "",
                      onSaved: (val) =>
                          setState(() => widget.task.name = val),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 16.0),
                      child: RaisedButton(
                        onPressed: () {
                          final form = _formKey.currentState;
                          form.save();
                          widget.task.save();
                        },
                        child: Text('Save'),
                      ),
                    ),
                  ]
              )
          )
      );
    } else {
      return Container(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget> [
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 20),
                  child: Text(widget.task.name),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 30, 0, 20),
                  child: Text('Done with no interrupt : ' + widget.task.count.toString()),
                ),
              ]
          )
      );
    }
  }


}

class TaskWidget extends StatefulWidget {
  final Task task;
  TaskWidget(this.task);

  @override
  TaskState createState() => TaskState();
}
