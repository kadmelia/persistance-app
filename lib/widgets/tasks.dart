import 'package:flutter/material.dart';
import 'package:persistance_app/models/Task.dart';
import 'package:persistance_app/widgets/task.dart';

class TasksState extends State<TasksWidget> {
  List<Task> _tasks = List<Task>();

  @override
  Widget build(BuildContext context) {
    _retrieveTasks();

    return Scaffold(
      appBar: AppBar(
        title: Text('List'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add), onPressed: _pushAddTask),
        ],
      ),
      body: _buildTasks(),
    );
  }

  Future<void> _retrieveTasks() async {
    List<Task> tasks = await Task.getAll();
    setState(() {
      _tasks = tasks;
    });
  }

  void _pushAddTask() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return TaskWidget(Task(
            id: 0,
            name: '',
            count: 0,
          ));
        },
      ),
    );
  }

  void _pushViewTask(Task task) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          return TaskWidget(task);
        },
      ),
    );
  }

  Widget _buildTasks() {
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: _tasks.length,
        itemBuilder: /*1*/ (context, i) {
          return _buildRow(_tasks[i]);
        });
  }
  Widget _buildRow(Task task) {
    return ListTile(
      title: Text(task.name),
      onTap: () {      // Add 9 lines from here...
        _pushViewTask(task);
      },
    );
  }
}

class TasksWidget extends StatefulWidget {
  @override
  TasksState createState() => TasksState();
}