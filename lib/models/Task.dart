import 'package:persistance_app/db.dart';
import 'package:sqflite/sqflite.dart';

class Task {
  int id;
  String name;
  int count;

  Task({this.name, this.count , this.id});

  // Convert a Task into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'count': count,
    };
  }

  Future<void> save() async {
    // Get a reference to the database.
    final Database db = await getDatabase();

    // Insert the Task into the correct table. You might also specify the
    // `conflictAlgorithm` to use in case the same dog is inserted twice.
    //
    // In this case, replace any previous data.
    await db.insert(
      'tasks',
      toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  static Future<List<Task>> getAll() async {
    final Database db = await getDatabase();

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('tasks');

    return List.generate(maps.length, (i) {
      return Task(
        id: maps[i]['id'],
        name: maps[i]['name'],
        count: maps[i]['count'],
      );
    });

  }


}